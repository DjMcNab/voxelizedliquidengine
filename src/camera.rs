use glam::{IVec3, Mat3, Mat4, Vec2, Vec3, Vec4};

mod camera_look {
    use glam::{const_mat3, const_quat, const_vec2, Mat3, Quat, Vec2};

    //// Base rotation from world to head-on plus X into camera
    pub const HEAD_ON_ROT: Quat = const_quat!([0.37174803, 0.37174803, 0.6015009, -0.6015009]);

    //// Rotation to the clockwise axonometric view
    pub const AXON_CCW_ROT: Quat = const_quat!([0.4247082, 0.17591989, 0.33985114, -0.82047325]);

    //// Ditto for clockwise.
    pub const AXON_CW_ROT: Quat = const_quat!([0.17591989, 0.4247082, 0.82047325, -0.33985114]);

    //// Viewspace stretch for head-on X into camera
    pub const HEAD_ON_STRETCH: Vec2 = const_vec2!([1., 0.8944273]);

    //// Viewspace stretch for axonometric view
    pub const AXON_STRETCH: Vec2 = const_vec2!([0.98994946, 0.9797959]);

    //// Counterclockwise worldspace rotation by 90 degrees around +Z
    pub const CCW_MAT: Mat3 = const_mat3!([0., 1., 0., -1., 0., 0., 0., 0., 1.]);
    //// Ditto clockwise.
    pub const CW_MAT: Mat3 = const_mat3!([0., -1., 0., 1., 0., 0., 0., 0., 1.]);
}

#[derive(Copy, Clone, PartialEq)]
enum TurntableState {
    StableHeadOn,
    StableAxonometricCCW,
    TransitCCWFromHeadOn,
    TransitCCWToHeadOn,
    TransitCWFromHeadOn,
    TransitCWToHeadOn,
}

struct Turntable {
    state: TurntableState,
    // Refers to a 'head-on' state implicitly referenced by the current state.
    permutation: Mat3,
    time: f32,
}

impl Turntable {
    fn new(permutation: Mat3) -> Self {
        Self {
            state: TurntableState::StableHeadOn,
            permutation,
            time: 0.0,
        }
    }

    fn is_stable(&self) -> bool {
        self.state == TurntableState::StableHeadOn
            || self.state == TurntableState::StableAxonometricCCW
    }

    fn rotate_ccw(&mut self) {
        use TurntableState::*;
        match self.state {
            StableHeadOn => {
                self.state = TransitCCWFromHeadOn;
                self.time = 0.0;
            }
            StableAxonometricCCW => {
                self.state = TransitCCWToHeadOn;
                self.permutation = self.permutation * camera_look::CCW_MAT;
                self.time = 0.0;
            }
            TransitCCWFromHeadOn | TransitCCWToHeadOn => {}
            TransitCWFromHeadOn => {
                self.state = TransitCCWToHeadOn;
                self.time = 1.0 - self.time;
            }
            TransitCWToHeadOn => {
                self.state = TransitCCWFromHeadOn;
                self.time = 1.0 - self.time;
            }
        }
    }

    fn rotate_cw(&mut self) {
        use TurntableState::*;
        match self.state {
            StableHeadOn => {
                self.state = TransitCWFromHeadOn;
                self.time = 0.0;
            }
            StableAxonometricCCW => {
                self.state = TransitCWToHeadOn;
                self.time = 0.0;
            }
            TransitCWFromHeadOn | TransitCWToHeadOn => {}
            TransitCCWFromHeadOn => {
                self.state = TransitCWToHeadOn;
                self.time = 1.0 - self.time;
            }
            TransitCCWToHeadOn => {
                self.state = TransitCWFromHeadOn;
                self.time = 1.0 - self.time;
            }
        }
    }

    fn time_passes(&mut self, dt: f32) {
        self.time = 1.0f32.min(self.time + dt);
        if self.time == 1.0 {
            use TurntableState::*;
            match self.state {
                TransitCWToHeadOn | TransitCCWToHeadOn => {
                    self.state = StableHeadOn;
                }
                TransitCCWFromHeadOn => {
                    self.state = StableAxonometricCCW;
                }
                TransitCWFromHeadOn => {
                    self.state = StableAxonometricCCW;
                    self.permutation = self.permutation * camera_look::CW_MAT;
                }
                _ => {}
            }
        }
    }

    fn view(&self) -> Mat3 {
        let q;
        let stretch;
        use camera_look::*;
        use TurntableState::*;
        match self.state {
            StableHeadOn => {
                q = HEAD_ON_ROT;
                stretch = HEAD_ON_STRETCH;
            }
            StableAxonometricCCW => {
                q = AXON_CCW_ROT;
                stretch = AXON_STRETCH;
            }
            TransitCWFromHeadOn => {
                q = HEAD_ON_ROT.slerp(AXON_CW_ROT, self.time);
                stretch = HEAD_ON_STRETCH.lerp(AXON_STRETCH, self.time);
            }
            TransitCCWFromHeadOn => {
                q = HEAD_ON_ROT.slerp(AXON_CCW_ROT, self.time);
                stretch = HEAD_ON_STRETCH.lerp(AXON_STRETCH, self.time);
            }
            TransitCWToHeadOn => {
                q = AXON_CCW_ROT.slerp(HEAD_ON_ROT, self.time);
                stretch = AXON_STRETCH.lerp(HEAD_ON_STRETCH, self.time);
            }
            TransitCCWToHeadOn => {
                q = AXON_CW_ROT.slerp(HEAD_ON_ROT, self.time);
                stretch = AXON_STRETCH.lerp(HEAD_ON_STRETCH, self.time);
            }
        }

        let mut res =
            Mat3::from_diagonal(stretch.extend(-1.0)) * Mat3::from_quat(q) * self.permutation;

        if self.is_stable() {
            // Normalize x, y components of the columns to decimals
            let m = res.as_mut();
            for i in 0..9 {
                if i % 3 != 2 {
                    m[i] = (m[i] * 10.0).round() / 10.0;
                }
            }
        }

        res
    }
}

use liquid_voxels_spir::chunklib::CHUNK_SIZE;
use std::cmp::min;
use winit::dpi::PhysicalSize;

pub struct CameraSettings {
    pub distance_to_camera: f32,
    pub distance_to_far_plane: f32,
    pub perspective: f32,
    pub screen_size: PhysicalSize<u32>,
    pub unit_size: f32,
}

impl CameraSettings {
    pub fn projection(&self) -> Mat4 {
        let zscale = 1.0 / (self.distance_to_camera + self.distance_to_far_plane);
        Mat4::from_cols_array_2d(&[
            [
                self.unit_size * 2.0 / self.screen_size.width as f32,
                0.0,
                0.0,
                0.0,
            ],
            [
                0.0,
                self.unit_size * 2.0 / self.screen_size.height as f32,
                0.0,
                0.0,
            ],
            [0.0, 0.0, zscale, self.perspective],
            [0.0, 0.0, self.distance_to_camera * zscale, 1.0],
        ])
    }

    pub fn pixel_space_inverse(&self) -> Mat4 {
        let us = self.unit_size;
        let sw = self.screen_size.width as f32;
        let sh = self.screen_size.height as f32;
        let p = self.perspective;
        let dc = self.distance_to_camera;
        let d = self.distance_to_camera + self.distance_to_far_plane;
        let denom = 1.0 - p * dc;
        debug_assert!(denom > 0.0);
        Mat4::from_cols_array_2d(&[
            [1.0 / us, 0.0, 0.0, 0.0],
            [0.0, -1.0 / us, 0.0, 0.0],
            [0.0, 0.0, d / denom, -p * d / denom],
            [-sw / 2.0 / us, sh / 2.0 / us, -dc / denom, 1.0 / denom],
        ])
    }
}

pub struct Camera {
    turntable: Turntable,
    cached_view: Mat4,
    cached_projection: Mat4,
    cached_pixspace: Mat4,
    pub look_at: Vec3,
}

fn expand(view: Mat3) -> Mat4 {
    let v = view.to_cols_array();
    Mat4::from_cols_array(&[
        v[0], v[1], v[2], 0.0, v[3], v[4], v[5], 0.0, v[6], v[7], v[8], 0.0, 0.0, 0.0, 0.0, 1.0,
    ])
}

impl Camera {
    pub fn new(look_at: Vec3, settings: &CameraSettings) -> Self {
        let turntable = Turntable::new(Mat3::IDENTITY);
        let view = turntable.view();
        Self {
            turntable,
            look_at,
            cached_projection: settings.projection(),
            cached_pixspace: settings.pixel_space_inverse(),
            cached_view: expand(view),
        }
    }

    pub fn set_settings(&mut self, settings: &CameraSettings) {
        self.cached_projection = settings.projection();
        self.cached_pixspace = settings.pixel_space_inverse();
    }

    pub fn rotate_cw(&mut self) {
        self.turntable.rotate_cw()
    }

    pub fn rotate_ccw(&mut self) {
        self.turntable.rotate_ccw()
    }

    pub fn time_passes(&mut self, dt: f32) {
        self.turntable.time_passes(dt);
        self.cached_view = expand(self.turntable.view());
    }

    pub fn forward(&self) -> Vec3 {
        self.cached_view.row(2).truncate()
    }

    pub fn is_stable(&self) -> bool {
        self.turntable.is_stable()
    }

    pub fn get_global_data(&self) -> liquid_voxels_spir::RayTracingGlobalData {
        let world_to_projection =
            self.cached_projection * self.cached_view * Mat4::from_translation(-self.look_at);
        let inv_dir = world_to_projection.inverse() * Vec4::Z;
        liquid_voxels_spir::RayTracingGlobalData {
            world_to_projection,
            inverse_direction: inv_dir / inv_dir.abs().max_element(),
            /*
            window_to_world: Mat4::from_translation(self.look_at)
                * self.cached_view.transpose()
                * self.cached_pixspace,
             */
        }
    }

    pub fn viewable_chunks(&self) -> (IVec3, IVec3, impl Fn(IVec3) -> bool) {
        let world_to_projection =
            self.cached_projection * self.cached_view * Mat4::from_translation(-self.look_at);
        let chunkspace_to_projection =
            world_to_projection * Mat4::from_scale(Vec3::splat(CHUNK_SIZE as f32));
        let projection_to_chunkspace = chunkspace_to_projection.inverse();

        use glam::f32::{vec3, vec4};
        use std::cmp::{Ordering, PartialOrd};

        let corner_points = [
            projection_to_chunkspace.project_point3(vec3(-1.0, -1.0, 0.0)),
            projection_to_chunkspace.project_point3(vec3(-1.0, -1.0, 1.0)),
            projection_to_chunkspace.project_point3(vec3(1.0, -1.0, 0.0)),
            projection_to_chunkspace.project_point3(vec3(1.0, -1.0, 1.0)),
            projection_to_chunkspace.project_point3(vec3(-1.0, 1.0, 0.0)),
            projection_to_chunkspace.project_point3(vec3(-1.0, 1.0, 1.0)),
            projection_to_chunkspace.project_point3(vec3(1.0, 1.0, 0.0)),
            projection_to_chunkspace.project_point3(vec3(1.0, 1.0, 1.0)),
        ];

        let corner_min = corner_points
            .iter()
            .map(|&x| x)
            .reduce(|a, b| a.min(b))
            .unwrap()
            .ceil()
            .as_i32()
            - IVec3::ONE;
        let corner_max = corner_points
            .iter()
            .map(|&x| x)
            .reduce(|a, b| a.max(b))
            .unwrap()
            .floor()
            .as_i32();

        let projection_adjspace = chunkspace_to_projection.transpose();

        let mut planes = [
            projection_adjspace * vec4(0.0, 0.0, 1.0, 0.0),
            projection_adjspace * vec4(0.0, 0.0, -1.0, 1.0),
            projection_adjspace * vec4(1.0, 0.0, 0.0, 1.0),
            projection_adjspace * vec4(-1.0, 0.0, 0.0, 1.0),
            projection_adjspace * vec4(0.0, 1.0, 0.0, 1.0),
            projection_adjspace * vec4(0.0, -1.0, 0.0, 1.0),
        ];

        // Shift the planes facing in +x, +y, or +z direction so that they admit shifts of -1 to all of them
        for i in 0..planes.len() {
            let mov = planes[i].truncate().max(Vec3::ZERO).dot(Vec3::ONE);
            planes[i].w += mov;
        }

        let check_ivec3 = move |x: IVec3| {
            let mut x = x.as_f32().extend(1.0);
            for plane in planes.iter() {
                if !(plane.dot(x) >= 0.0) {
                    return false;
                }
            }
            true
        };

        (corner_min, corner_max, check_ivec3)
    }
}
