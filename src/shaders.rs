use liquid_voxels_spir::RayTracingGlobalData;
use wgpu::*;

pub struct Shaders {
    pub block_cuboid: wgpu::ShaderModule,
    pub raytrace: wgpu::ShaderModule,
    pub test_vert: wgpu::ShaderModule,
    pub test_frag: wgpu::ShaderModule,
}

fn load(device: &wgpu::Device, desc: &wgpu::ShaderModuleDescriptorSpirV) -> wgpu::ShaderModule {
    unsafe { device.create_shader_module_spirv(desc) }
}
impl Shaders {
    pub fn new(device: &wgpu::Device) -> Self {
        let test_vert = unsafe {
            device.create_shader_module_spirv(&include_spirv_raw!(env!("test_vert.vert.spv")))
        };
        let test_frag = unsafe {
            device.create_shader_module_spirv(&include_spirv_raw!(env!("test_frag.frag.spv")))
        };
        let block_cuboid = load(device, &include_spirv_raw!(env!("entry_block_cuboid")));
        let raytrace = load(device, &include_spirv_raw!(env!("entry_raytrace")));
        Self {
            block_cuboid,
            raytrace,
            test_vert,
            test_frag,
        }
    }
}

pub fn create_pipeline_layout(
    device: &Device,
    chunks: &mut crate::voxel_data::Chunks,
) -> PipelineLayout {
    let desc = PipelineLayoutDescriptor {
        label: Some("default layout"),
        bind_group_layouts: &[chunks.get_layout()],
        push_constant_ranges: &[PushConstantRange {
            stages: ShaderStage::VERTEX_FRAGMENT,
            range: 0..(std::mem::size_of::<RayTracingGlobalData>() as u32),
        }],
    };
    device.create_pipeline_layout(&desc)
}

pub fn create_render_pipeline(
    device: &Device,
    layout: &PipelineLayout,
    shaders: &Shaders,
    targets: &[ColorTargetState],
) -> RenderPipeline {
    let desc = RenderPipelineDescriptor {
        label: Some("raytracing"),
        layout: Some(layout),
        vertex: VertexState {
            module: &shaders.block_cuboid,
            entry_point: "block_cuboid",
            buffers: &[],
        },
        primitive: PrimitiveState {
            topology: PrimitiveTopology::TriangleList,
            strip_index_format: None,
            cull_mode: Some(Face::Front),
            front_face: FrontFace::Cw,
            conservative: true,
            ..Default::default()
        },
        depth_stencil: Some(DepthStencilState {
            format: TextureFormat::Depth24PlusStencil8,
            depth_write_enabled: true,
            depth_compare: CompareFunction::Less,
            stencil: Default::default(),
            bias: Default::default(),
        }),
        multisample: Default::default(),
        fragment: Some(FragmentState {
            module: &shaders.raytrace,
            entry_point: "raytrace",
            targets,
        }),
    };
    device.create_render_pipeline(&desc)
}

pub fn create_early_depth_pipeline(device: &Device, shaders: &Shaders) -> RenderPipeline {
    let desc = RenderPipelineDescriptor {
        label: Some("early depth"),
        layout: None,
        vertex: VertexState {
            module: &shaders.block_cuboid,
            entry_point: "block_cuboid",
            buffers: &[],
        },
        primitive: PrimitiveState {
            topology: PrimitiveTopology::TriangleStrip,
            strip_index_format: Some(IndexFormat::Uint16),
            cull_mode: Some(Face::Front),
            conservative: true,
            ..Default::default()
        },
        depth_stencil: Some(DepthStencilState {
            format: TextureFormat::Depth24PlusStencil8,
            stencil: StencilState {
                back: StencilFaceState {
                    compare: CompareFunction::Never,
                    fail_op: StencilOperation::Keep,
                    depth_fail_op: StencilOperation::Keep,
                    pass_op: StencilOperation::Keep,
                },
                front: StencilFaceState {
                    compare: CompareFunction::Always,
                    fail_op: StencilOperation::Keep,
                    depth_fail_op: StencilOperation::Zero,
                    pass_op: StencilOperation::Replace,
                },
                read_mask: 0,
                write_mask: 255,
            },
            depth_compare: CompareFunction::Less,
            depth_write_enabled: false,
            bias: Default::default(),
        }),
        multisample: Default::default(),
        fragment: None,
    };
    device.create_render_pipeline(&desc)
}
