use wgpu::{
    ColorTargetState, Device, RenderPassColorAttachment, RenderPassDepthStencilAttachment,
    RenderPassDescriptor, Texture, TextureDescriptor, TextureDimension, TextureFormat,
    TextureUsage, TextureView,
};
use winit::dpi::PhysicalSize;

#[derive(Copy, Clone, PartialEq)]
pub enum DebugShow {
    None,
    Normal,
    Offset,
    RayDir,
}

struct Target {
    desc: wgpu::TextureDescriptor<'static>,
    texture: wgpu::Texture,
    default_view: wgpu::TextureView,
    debug_show: Option<DebugShow>,
}

pub struct Targets {
    size: winit::dpi::PhysicalSize<u32>,
    normal: Target,
    cell: Target,
    offset: Target,
    ray_dir: Target,
    depth_stencil: Target,
}

impl Target {
    fn new(
        device: &Device,
        debug_show: Option<DebugShow>,
        desc: TextureDescriptor<'static>,
    ) -> Self {
        let texture = device.create_texture(&desc);
        let default_view = texture.create_view(&Default::default());
        Self {
            desc,
            texture,
            default_view,
            debug_show,
        }
    }

    fn debug_elide(&self, current_debug: DebugShow) -> Option<&Self> {
        match self.debug_show {
            Some(debug) if debug == current_debug => None,
            _ => Some(self),
        }
    }

    fn color_target_state(
        &self,
        current_debug: DebugShow,
        default: &ColorTargetState,
    ) -> ColorTargetState {
        self.debug_elide(current_debug).map_or_else(
            || default.clone(),
            |tar| ColorTargetState {
                format: tar.desc.format,
                ..*default
            },
        )
    }

    fn render_pass_color_attachment<'a, 'b>(
        &'a self,
        current_debug: DebugShow,
        default: &'b RenderPassColorAttachment<'a>,
    ) -> RenderPassColorAttachment<'a> {
        self.debug_elide(current_debug).map_or_else(
            || default.clone(),
            |tar| RenderPassColorAttachment {
                view: &tar.default_view,
                ..*default
            },
        )
    }
}

fn with_default_view(t: Texture) -> (Texture, TextureView) {
    let view = t.create_view(&Default::default());
    (t, view)
}

impl Targets {
    pub fn new(size: PhysicalSize<u32>, device: &Device) -> Self {
        let extent = wgpu::Extent3d {
            width: size.width,
            height: size.height,
            depth_or_array_layers: 1,
        };
        let normal = Target::new(
            device,
            Some(DebugShow::Normal),
            TextureDescriptor {
                label: Some("normal buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba8Snorm,
                usage: TextureUsage::RENDER_ATTACHMENT | TextureUsage::SAMPLED,
            },
        );
        let cell = Target::new(
            device,
            None,
            TextureDescriptor {
                label: Some("cell buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba32Sint,
                usage: TextureUsage::RENDER_ATTACHMENT | TextureUsage::SAMPLED,
            },
        );
        let offset = Target::new(
            device,
            Some(DebugShow::Offset),
            TextureDescriptor {
                label: Some("normal buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba8Unorm,
                usage: TextureUsage::RENDER_ATTACHMENT | TextureUsage::SAMPLED,
            },
        );
        let ray_dir = Target::new(
            device,
            Some(DebugShow::RayDir),
            TextureDescriptor {
                label: Some("normal buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba32Float,
                usage: TextureUsage::RENDER_ATTACHMENT | TextureUsage::SAMPLED,
            },
        );
        let depth_stencil = Target::new(
            device,
            None,
            TextureDescriptor {
                label: Some("normal buffer"),
                size: extent,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Depth24PlusStencil8,
                usage: TextureUsage::RENDER_ATTACHMENT | TextureUsage::SAMPLED,
            },
        );
        Self {
            size,
            normal,
            cell,
            offset,
            ray_dir,
            depth_stencil,
        }
    }

    fn canonical_color_targets(&self) -> [&Target; 4] {
        [&self.normal, &self.cell, &self.offset, &self.ray_dir]
    }

    pub fn color_target_states(
        &self,
        debug: DebugShow,
        output_format: TextureFormat,
    ) -> [wgpu::ColorTargetState; 4] {
        let output_format_desc = ColorTargetState {
            format: output_format,
            write_mask: wgpu::ColorWrite::ALL,
            blend: None,
        };

        [
            self.normal.color_target_state(debug, &output_format_desc),
            self.cell.color_target_state(debug, &output_format_desc),
            self.offset.color_target_state(debug, &output_format_desc),
            self.ray_dir.color_target_state(debug, &output_format_desc),
        ]
    }

    pub fn begin_render_pass<'a>(
        &'a self,
        encoder: &'a mut wgpu::CommandEncoder,
        debug: DebugShow,
        output: &'a TextureView,
    ) -> wgpu::RenderPass<'a> {
        let output_attachment = RenderPassColorAttachment {
            view: output,
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(wgpu::Color {
                    r: 0.0,
                    g: 0.0,
                    b: 0.0,
                    a: 0.0,
                }),
                store: true,
            },
        };

        let attachments: heapless::Vec<_, 4> = self
            .canonical_color_targets()
            .iter()
            .map(|&target| target.render_pass_color_attachment(debug, &output_attachment))
            .collect();

        encoder.begin_render_pass(&RenderPassDescriptor {
            label: None,
            color_attachments: &attachments,
            depth_stencil_attachment: Some(RenderPassDepthStencilAttachment {
                view: &self.depth_stencil.default_view,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: None,
            }),
        })
    }
}
