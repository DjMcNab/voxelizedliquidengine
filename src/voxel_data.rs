use glam::{const_uvec3, IVec3, IVec4, UVec3, UVec4};
use heapless::Vec as HeaplessVec;
use liquid_voxels_spir::chunklib::*;
use std::num::NonZeroU32;
use std::ops::Range;
use wgpu::*;

#[derive(Copy, Clone)]
pub enum DataFormat {
    Float,
    StaggeredFloat,
}

pub struct AtlasRepository {
    texture_size: UVec3,
    atlas_0: HeaplessVec<(Texture, TextureView), MAX_ATLAS>,
    atlas_1: HeaplessVec<(Texture, TextureView), MAX_ATLAS>,
    flip: bool,
}

impl AtlasRepository {
    pub fn new(texture_size: UVec3) -> Self {
        Self {
            texture_size,
            atlas_0: HeaplessVec::new(),
            atlas_1: HeaplessVec::new(),
            flip: false,
        }
    }

    pub fn add_atlas(&mut self, device: &Device, label: &'static str) -> Option<usize> {
        if self.atlas_0.len() < self.atlas_0.capacity() {
            let desc = TextureDescriptor {
                label: Some(label),
                dimension: TextureDimension::D3,
                size: Extent3d {
                    width: self.texture_size.x,
                    height: self.texture_size.y,
                    depth_or_array_layers: self.texture_size.z,
                },
                format: TextureFormat::Rgba32Float,
                mip_level_count: 1,
                sample_count: 1,
                usage: TextureUsage::COPY_DST | TextureUsage::SAMPLED | TextureUsage::STORAGE,
            };
            let tex0 = device.create_texture(&desc);
            let view0 = tex0.create_view(&Default::default());
            self.atlas_0.push((tex0, view0)).unwrap();
            let tex1 = device.create_texture(&desc);
            let view1 = tex1.create_view(&Default::default());
            self.atlas_1.push((tex1, view1)).unwrap();
            Some(self.atlas_0.len() - 1)
        } else {
            None
        }
    }
}

const CHUNK_SIZE_WITH_APRONS: u32 = 18;

pub struct AtlasCells {
    texture_size: UVec3,
    available_items: Vec<UVec3>,
}

impl AtlasCells {
    pub fn new(texture_size: UVec3) -> Self {
        let cells = texture_size / CHUNK_SIZE_WITH_APRONS;
        let mut items = Vec::new();
        let all_cells = (0..cells.x)
            .into_iter()
            .flat_map(|x| {
                (0..cells.y).into_iter().flat_map(move |y| {
                    (0..cells.z)
                        .into_iter()
                        .map(move |z| UVec3::new(x, y, z) * CHUNK_SIZE_WITH_APRONS + UVec3::ONE)
                })
            })
            .rev();
        items.extend(all_cells);
        Self {
            texture_size,
            available_items: items,
        }
    }

    pub fn allocate_cell(&mut self) -> Option<UVec3> {
        self.available_items.pop()
    }

    pub fn reclaim_cell(&mut self, cell: UVec3) {
        self.available_items.push(cell)
    }
}

pub type AtlasInitializer = Box<dyn FnMut(IVec3, UVec3, &Texture, &Queue)>;

pub struct VoxelDataBuilder<'a> {
    device: &'a Device,
    atlases: AtlasRepository,
    initializers: HeaplessVec<AtlasInitializer, MAX_ATLAS>,
    material_data: Option<usize>,
    texture_size: UVec3,
}

pub struct Chunks {
    pub backing_data: ChunkBackingData,
    backing_data_buffer: Buffer,
    dirty: bool,
    atlases: AtlasRepository,
    initializers: HeaplessVec<AtlasInitializer, MAX_ATLAS>,
    atlas_cells: AtlasCells,
    bind_layout: BindGroupLayout,
}

impl<'a> VoxelDataBuilder<'a> {
    pub fn new(texture_size: UVec3, device: &'a Device) -> Self {
        Self {
            device,
            atlases: AtlasRepository::new(texture_size),
            initializers: HeaplessVec::new(),
            material_data: None,
            texture_size,
        }
    }

    fn add_atlas(&mut self, label: &'static str, initializer: AtlasInitializer) -> Option<usize> {
        let r = self.atlases.add_atlas(self.device, label)?;
        unsafe { self.initializers.push_unchecked(initializer) };
        Some(r)
    }

    pub fn material_data(mut self, initializer: AtlasInitializer) -> Self {
        self.material_data = self.add_atlas("material_data", initializer);
        self
    }

    pub fn build(self) -> Chunks {
        let atlasref = StandardAtlases {
            material_data: self.material_data.expect("material data atlas is missing") as u32,
        };
        let backing_data = ChunkBackingData {
            num_chunks: 0,
            chunk_flags: [0u32; MAX_CHUNKS],
            world_cells: [IVec4::ZERO; MAX_CHUNKS],
            atlas_origins: [UVec4::ZERO; MAX_CHUNKS],
            hash_lookup: [u32::MAX; HASH_SIZE],
            atlases: atlasref,
        };
        let backing_data_buffer = self.device.create_buffer(&BufferDescriptor {
            label: Some("chunk backing data"),
            size: std::mem::size_of::<ChunkBackingData>() as u64,
            usage: BufferUsage::STORAGE | BufferUsage::COPY_DST,
            mapped_at_creation: false,
        });
        let atlases = self.atlases;
        let atlas_cells = AtlasCells::new(self.texture_size);
        let initializers = self.initializers;
        let bind_layout = Chunks::create_common_bind_layout(atlases.atlas_0.len(), self.device);
        Chunks {
            backing_data,
            backing_data_buffer,
            atlases,
            atlas_cells,
            initializers,
            bind_layout,
            dirty: false,
        }
    }
}

impl Chunks {
    pub fn add_chunk(&mut self, cell: IVec3, queue: &Queue) -> Option<u32> {
        let mut x: ChunkFound = Default::default();
        if self.backing_data.find_chunk(cell, &mut x) {
            return Some(x.id);
        }
        if (self.backing_data.num_chunks as usize) < MAX_CHUNKS {
            let n = self.backing_data.num_chunks as usize;
            let atlas_cell = self.atlas_cells.allocate_cell()?;
            self.backing_data.world_cells[n] = cell.extend(0);
            self.backing_data.atlas_origins[n] = atlas_cell.extend(0);
            self.backing_data.chunk_flags[n] = 0u32;
            self.backing_data.num_chunks += 1;
            for (i, init) in self.initializers.iter_mut().enumerate() {
                init(cell, atlas_cell, &self.atlases.atlas_0[i].0, queue);
                init(cell, atlas_cell, &self.atlases.atlas_1[i].0, queue);
            }
            self.rehash();
            self.dirty = true;
            return Some(n as u32);
        }
        None
    }

    pub fn remove_chunk(&mut self, cell: IVec3) -> bool {
        let mut x: ChunkFound = Default::default();
        if !self.backing_data.find_chunk(cell, &mut x) {
            return false;
        }
        self.remove_chunk_by_index(x.id);
        true
    }

    fn remove_chunk_by_index(&mut self, index: u32) {
        let idx = index as usize;
        let last = (self.backing_data.num_chunks - 1) as usize;
        self.backing_data.world_cells[idx] = self.backing_data.world_cells[last];
        self.backing_data.atlas_origins[idx] = self.backing_data.atlas_origins[last];
        self.backing_data.chunk_flags[idx] = self.backing_data.chunk_flags[last];
        self.backing_data.num_chunks -= 1;
        self.rehash();
        self.dirty = true;
    }

    pub fn set_chunk_flags(&mut self, cell: IVec3, flags: u32) -> bool {
        let mut x: ChunkFound = Default::default();
        if !self.backing_data.find_chunk(cell, &mut x) {
            return false;
        }
        self.backing_data.chunk_flags[x.id as usize] = flags;
        self.dirty = true;
        true
    }

    pub fn refresh(&mut self, queue: &Queue) {
        if self.dirty {
            self.rehash();
            queue.write_buffer(&self.backing_data_buffer, 0, unsafe {
                std::mem::transmute::<_, &[u8; std::mem::size_of::<ChunkBackingData>()]>(
                    &self.backing_data,
                )
            });
            self.dirty = false;
        }
    }

    pub fn get_layout(&self) -> &BindGroupLayout {
        &self.bind_layout
    }

    fn create_common_bind_layout(num_atlases: usize, device: &Device) -> BindGroupLayout {
        let bind0 = BindGroupLayoutDescriptor {
            label: Some("Chunk data"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStage::VERTEX_FRAGMENT | ShaderStage::COMPUTE,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Storage { read_only: false },
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(
                            std::mem::size_of::<ChunkBackingData>() as u64
                        ),
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStage::VERTEX_FRAGMENT | ShaderStage::COMPUTE,
                    ty: BindingType::Texture {
                        view_dimension: TextureViewDimension::D3,
                        sample_type: TextureSampleType::Float { filterable: true },
                        multisampled: false,
                    },
                    count: NonZeroU32::new(num_atlases as u32),
                },
                BindGroupLayoutEntry {
                    binding: 2,
                    visibility: ShaderStage::FRAGMENT | ShaderStage::COMPUTE,
                    ty: BindingType::StorageTexture {
                        view_dimension: TextureViewDimension::D3,
                        format: TextureFormat::Rgba32Float,
                        access: StorageTextureAccess::ReadWrite,
                    },
                    count: NonZeroU32::new(num_atlases as u32),
                },
            ],
        };
        device.create_bind_group_layout(&bind0)
    }

    pub fn get_bind_group(&self, device: &Device) -> BindGroup {
        let ref atlas_fw;
        let ref atlas_bk;
        if self.atlases.flip {
            atlas_fw = &self.atlases.atlas_0;
            atlas_bk = &self.atlases.atlas_1;
        } else {
            atlas_fw = &self.atlases.atlas_1;
            atlas_bk = &self.atlases.atlas_0;
        }

        let views_fw: HeaplessVec<_, 10> = atlas_fw.iter().map(|(_, ref v)| v).collect();
        let views_bk: HeaplessVec<_, 10> = atlas_bk.iter().map(|(_, ref v)| v).collect();

        let desc = BindGroupDescriptor {
            label: None,
            layout: &self.bind_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: self.backing_data_buffer.as_entire_binding(),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::TextureViewArray(&views_fw[..]),
                },
                BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureViewArray(&views_bk[..]),
                },
            ],
        };

        device.create_bind_group(&desc)
    }

    pub fn cuboid_draw_range(&self) -> Range<u32> {
        0..(self.backing_data.num_chunks * 36)
    }

    pub fn delete_chunks(&mut self, mut filter: impl FnMut(IVec3) -> bool) {
        for i in (0..self.backing_data.num_chunks).rev() {
            if !filter(self.backing_data.world_cells[i as usize].truncate()) {
                self.remove_chunk_by_index(i);
            }
        }
    }

    pub fn sort_chunks(&mut self, cameradata: &liquid_voxels_spir::RayTracingGlobalData) {
        let mut s: HeaplessVec<_, MAX_CHUNKS> =
            (0..self.backing_data.num_chunks as usize).collect();
        s.sort_by(|&i, &j| {
            let zi = cameradata
                .world_to_projection
                .project_point3(
                    self.backing_data.world_cells[i].truncate().as_f32() * CHUNK_SIZE as f32,
                )
                .z;
            let zj = cameradata
                .world_to_projection
                .project_point3(
                    self.backing_data.world_cells[i].truncate().as_f32() * CHUNK_SIZE as f32,
                )
                .z;
            zi.partial_cmp(&zj).unwrap()
        });

        let mut new_data = self.backing_data.clone();
        for (i, j) in s.into_iter().enumerate() {
            new_data.world_cells[i] = self.backing_data.world_cells[i];
            new_data.atlas_origins[i] = self.backing_data.atlas_origins[i];
            new_data.chunk_flags[i] = self.backing_data.chunk_flags[i];
        }
        self.backing_data = new_data;
        self.rehash();
        self.dirty = true;
    }

    fn rehash(&mut self) {
        for i in 0..HASH_SIZE {
            self.backing_data.hash_lookup[i] = u32::MAX;
        }

        let hasher = |v: IVec4| hash_cell(v.truncate());

        let hash_size = HASH_SIZE as u32;

        let mut max_offset = 0;
        for mut i in 0..self.backing_data.num_chunks {
            let mut h = hasher(self.backing_data.world_cells[i as usize]);
            let mut offset = 0;
            while offset < HASH_LEN {
                let idx = (h + offset) % hash_size;
                match self.backing_data.hash_lookup[idx as usize] {
                    u32::MAX => {
                        self.backing_data.hash_lookup[idx as usize] = i;
                        break;
                    }
                    i_other => {
                        let hash_other = hasher(self.backing_data.world_cells[i_other as usize]);
                        let offset_other = ((hash_other + hash_size) - idx) % hash_size;
                        if offset_other < offset {
                            self.backing_data.hash_lookup[idx as usize] = i;
                            i = i_other;
                            h = hash_other;
                            offset = offset_other;
                        }
                    }
                }
                offset += 1;
            }
            if offset == HASH_LEN {
                panic!("hash chain too long")
            }
            max_offset = max_offset.max(offset);
        }
        for i in 0..self.backing_data.num_chunks {
            let mut chunk = Default::default();
            assert!(self.backing_data.find_chunk(
                self.backing_data.world_cells[i as usize].truncate(),
                &mut chunk
            ))
        }
    }
}
