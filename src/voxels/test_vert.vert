#version 450 core

#extension GL_EXT_debug_printf: require

const int MAX_CHUNKS = 64;

layout(set=0, binding=0) buffer ChunkBackingData {
    uint num_chunks;
    uint chunk_flags[MAX_CHUNKS];
    ivec3 world_cells[MAX_CHUNKS];
    uvec3 atlas_origins[MAX_CHUNKS];
} chunk_backing_data;

layout(location=0) out vec4 position;

layout(push_constant) uniform RayTracingGlobalData {
    mat4 world_to_projection;
    mat4 window_to_world;
} global_data;

void main() {
    int vert_id = gl_VertexIndex;

    int chunk_id = vert_id / 36;
    int face_id = (vert_id % 36) / 6;
    int triangle_id = (vert_id % 6) / 3;
    int vertex_id = vert_id % 3;

    int dir = face_id % 3;
    int pos = face_id / 3;

    int nz = dir >> 1;
    int ny = dir & 1;
    int nx = 1 ^ (nz | ny);

    int flip = 1 - 2 * pos;

    ivec3 d = ivec3(nx, ny, nz);
    ivec3 n = d * flip;
    ivec3 u = -d.yzx;
    ivec3 v = d.zxy * flip;

    int mirror = 2 * triangle_id - 1;

    ivec3 xyz = n + u * (mirror * (1 - 2 * (vertex_id & 1))) + v * (mirror * (1 - 2 * (vertex_id >> 1)));
    ivec3 xyz_10 = (xyz + ivec3(1, 1, 1)) / 2;

    ivec3 vx = xyz_10 + (chunk_backing_data.world_cells[chunk_id] * 16);
    vec4 p = global_data.world_to_projection * vec4((vx * 16), 1.0);
    debugPrintfEXT("%d pos=%f %f %f %f\n", vert_id, p.x, p.y, p.z);
    gl_Position = p;
}