#![windows_subsystem = "console"]

extern crate bytemuck;
extern crate futures;
extern crate heapless;
extern crate liquid_voxels_spir;
extern crate wgpu;
extern crate winit;

mod camera;
mod shaders;
mod targets;
mod voxel_data;

use glam::uvec3;
use liquid_voxels_spir::RayTracingGlobalData;
use std::num::NonZeroU32;
use std::path::PathBuf;
use std::str::FromStr;
use std::time::{Duration, Instant};
use wgpu::{
    CommandEncoderDescriptor, Extent3d, ImageCopyTexture, ImageDataLayout, Origin3d,
    RenderPassDescriptor, ShaderStage, TextureAspect,
};
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

fn main() {
    use futures::executor::block_on;
    env_logger::init();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("MY Nice App")
        .build(&event_loop)
        .unwrap();

    let size = window.inner_size();
    let instance = wgpu::Instance::new(wgpu::BackendBit::VULKAN);

    let surface = unsafe { instance.create_surface(&window) };

    let adapter = block_on(instance.request_adapter(&wgpu::RequestAdapterOptions {
        power_preference: wgpu::PowerPreference::HighPerformance,
        compatible_surface: Some(&surface),
    }))
    .unwrap();

    let (device, queue) =
        block_on(adapter.request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                    | wgpu::Features::TEXTURE_BINDING_ARRAY
                    | wgpu::Features::PUSH_CONSTANTS
                    | wgpu::Features::SPIRV_SHADER_PASSTHROUGH
                    | wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                    | wgpu::Features::STORAGE_RESOURCE_BINDING_ARRAY
                    | wgpu::Features::CONSERVATIVE_RASTERIZATION
                    | wgpu::Features::VERTEX_WRITABLE_STORAGE
                    | wgpu::Features::UNSIZED_BINDING_ARRAY,
                limits: wgpu::Limits {
                    max_push_constant_size: std::mem::size_of::<
                        liquid_voxels_spir::RayTracingGlobalData,
                    >() as u32,
                    max_bind_groups: 3,
                    ..Default::default()
                },
            },
            Some(&PathBuf::from_str("./traces").unwrap()),
        ))
        .unwrap();

    let mut sc_desc = wgpu::SwapChainDescriptor {
        usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
        format: adapter.get_swap_chain_preferred_format(&surface).unwrap(),
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Fifo,
    };
    let mut swap_chain = device.create_swap_chain(&surface, &sc_desc);
    println!("Swapchain created!");
    let mut cam_desc = camera::CameraSettings {
        screen_size: size,
        unit_size: 16.,
        distance_to_camera: 128.,
        distance_to_far_plane: 128.,
        perspective: 0.,
    };
    let mut camera = camera::Camera::new(glam::vec3(8.0, 8.0, 16.0), &cam_desc);
    //camera.rotate_cw();
    //camera.time_passes(1.0);

    let mut chunks = voxel_data::VoxelDataBuilder::new(uvec3(512, 512, 18), &device)
        .material_data(Box::new(|chunk, atlas_origin, texture, queue| {
            if chunk.z > 0 {
                queue.write_texture(
                    ImageCopyTexture {
                        origin: Origin3d {
                            x: atlas_origin.x,
                            y: atlas_origin.y,
                            z: atlas_origin.z,
                        },
                        mip_level: 0,
                        texture,
                        aspect: TextureAspect::All,
                    },
                    bytemuck::cast_slice(&[[0.0f32; 4]; 16 * 16 * 16]),
                    ImageDataLayout {
                        offset: 0,
                        bytes_per_row: NonZeroU32::new(16 * 16),
                        rows_per_image: NonZeroU32::new(16),
                    },
                    Extent3d {
                        width: 16,
                        height: 16,
                        depth_or_array_layers: 16,
                    },
                );
            } else {
                queue.write_texture(
                    ImageCopyTexture {
                        origin: Origin3d {
                            x: atlas_origin.x,
                            y: atlas_origin.y,
                            z: atlas_origin.z,
                        },
                        mip_level: 0,
                        texture,
                        aspect: TextureAspect::All,
                    },
                    bytemuck::cast_slice(&[[1.0f32, 0.0f32, 0.0f32, 0.0f32]; 16 * 16 * 16]),
                    ImageDataLayout {
                        offset: 0,
                        bytes_per_row: NonZeroU32::new(16 * 16),
                        rows_per_image: NonZeroU32::new(16),
                    },
                    Extent3d {
                        width: 16,
                        height: 16,
                        depth_or_array_layers: 16,
                    },
                );
            }
        }))
        .build();

    let mut render_target = targets::Targets::new(size, &device);

    let shader = shaders::Shaders::new(&device);

    let debug = targets::DebugShow::Normal;
    let pipeline_layout = shaders::create_pipeline_layout(&device, &mut chunks);
    println!("pipeline layout success!");
    let pipeline = shaders::create_render_pipeline(
        &device,
        &pipeline_layout,
        &shader,
        &render_target.color_target_states(debug, sc_desc.format),
    );

    let mut moment = Instant::now();

    println!("Let's gooo!");
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            ref event,
            window_id,
        } if window_id == window.id() => match event {
            WindowEvent::Resized(new_size) => {
                sc_desc.width = new_size.width;
                sc_desc.height = new_size.height;
                swap_chain = device.create_swap_chain(&surface, &sc_desc);
                cam_desc.screen_size = *new_size;
                camera.set_settings(&cam_desc);
                render_target = targets::Targets::new(*new_size, &device);
            }
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            WindowEvent::KeyboardInput { input, .. } => match input {
                KeyboardInput {
                    state: ElementState::Pressed,
                    virtual_keycode: Some(keycode),
                    ..
                } => {
                    use VirtualKeyCode::*;
                    match keycode {
                        Escape => *control_flow = ControlFlow::Exit,
                        W => {
                            let fwd = camera.forward();
                            camera.look_at += glam::vec3(fwd.x, fwd.y, 0.0);
                            window.request_redraw();
                        }
                        S => {
                            let fwd = camera.forward();
                            camera.look_at += glam::vec3(-fwd.x, -fwd.y, 0.0);
                            window.request_redraw();
                        }
                        A => {
                            camera.rotate_ccw();
                        }
                        D => {
                            camera.rotate_cw();
                        }
                        Space => {
                            use glam::f32::*;
                            use glam::{IVec3, UVec3};
                            use liquid_voxels_spir::dda;
                            use liquid_voxels_spir::raylib::*;
                            let gdata = camera.get_global_data();
                            let in_world_pos = vec3(0.0, 0.0, 0.0);
                            let direction = gdata.inverse_direction.truncate()
                                - gdata.inverse_direction.w * in_world_pos;
                            let direction = direction.normalize();

                            let w0 = (gdata.world_to_projection * in_world_pos.extend(1.0)).w;
                            let z0 = (gdata.world_to_projection * in_world_pos.extend(1.0)).z;
                        }
                        _ => {}
                    }
                }
                _ => {}
            },
            _ => {}
        },
        Event::MainEventsCleared => {
            if !camera.is_stable() {
                window.request_redraw()
            }
            let now = Instant::now();
            camera.time_passes((now - moment).as_secs_f32() / 2.0);
            moment = now;
        }
        Event::RedrawRequested(window_id) if window.id() == window_id => {
            let (min, max, test) = camera.viewable_chunks();
            chunks.delete_chunks(&test);

            for x in min.x..=max.x {
                for y in min.y..=max.y {
                    for z in min.z..=0 {
                        let chunk = glam::IVec3::new(x, y, z);
                        if test(chunk) {
                            chunks
                                .add_chunk(chunk, &queue)
                                .expect(&format!("chunk {}, {}, {} failed", x, y, z));
                        }
                        if z == 0 {
                            chunks.set_chunk_flags(
                                chunk,
                                liquid_voxels_spir::chunklib::flags::VISIBLE,
                            );
                        } else {
                            chunks.set_chunk_flags(chunk, 0);
                        }
                    }
                }
            }

            let start = Instant::now();
            let gdata = camera.get_global_data();
            chunks.sort_chunks(&gdata);

            let draw_range = chunks.cuboid_draw_range();
            chunks.refresh(&queue);

            let group = chunks.get_bind_group(&device);

            let target = swap_chain.get_current_frame().unwrap();

            let mut encoder = device.create_command_encoder(&CommandEncoderDescriptor {
                label: Some("main render"),
            });
            {
                let mut pass =
                    render_target.begin_render_pass(&mut encoder, debug, &target.output.view);

                pass.set_pipeline(&pipeline);
                pass.set_bind_group(0, &group, &[]);
                let bytes_of_gdata = unsafe {
                    std::mem::transmute::<_, [u8; std::mem::size_of::<RayTracingGlobalData>()]>(
                        gdata,
                    )
                };
                pass.set_push_constants(ShaderStage::VERTEX_FRAGMENT, 0, &bytes_of_gdata);
                pass.draw(draw_range, 1..2);
            }
            queue.submit(std::iter::once(encoder.finish()));
            println!("Frame time: {}ms", (Instant::now() - start).as_millis())
        }
        _ => {}
    });
}
