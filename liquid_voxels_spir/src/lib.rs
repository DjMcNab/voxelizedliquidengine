#![no_std]
#![cfg_attr(target_arch = "spirv", feature(register_attr), register_attr(spirv))]
#![feature(stmt_expr_attributes, asm, const_generics, const_evaluatable_checked)]
//#![deny(warnings)]

pub mod chunklib;
pub mod dda;
pub mod finite_difference;
pub mod raylib;
pub mod spacecurve;

use chunklib::*;

extern crate glam;
extern crate spirv_std;
#[cfg(not(target_arch = "spirv"))]
#[allow(unused_imports)]
use spirv_std::macros::spirv;
use spirv_std::RuntimeArray;

use spirv_std::num_traits::float::Float;

use glam::{
    f32::{vec4, Vec3, Vec4},
    i32::{IVec3, IVec4},
    BVec3, Mat4, Vec3Swizzles, Vec4Swizzles,
};

#[repr(C)]
pub struct RayTracingGlobalData {
    pub world_to_projection: Mat4,
    pub inverse_direction: Vec4,
}

#[spirv(vertex)]
pub fn block_cuboid(
    #[spirv(vertex_index)] vert_id: u32,
    #[spirv(position)] out_pos: &mut Vec4,
    #[spirv(push_constant)] global_data: &RayTracingGlobalData,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] chunk_backing_data: &ChunkBackingData,
    #[spirv(invariant)] out_world_pos: &mut Vec3,
    #[spirv(flat)] out_chunk_id: &mut u32,
) {
    let chunk_id = vert_id / 36;
    let face_id = (vert_id % 36) / 6;
    let triangle_id = (vert_id % 6) / 3;
    let vertex_id = vert_id % 3;

    let dir = face_id % 3;
    let pos = face_id / 3;

    let nz = dir >> 1;
    let ny = dir & 1;
    let nx = 1 ^ (nz | ny);

    let flip = 1 - 2 * (pos as i32);

    let d = IVec3::new(nx as i32, ny as i32, nz as i32);
    let n = d * flip;
    let u = -d.yzx();
    let v = d.zxy() * flip;

    let mirror = 2 * (triangle_id as i32) - 1;

    let xyz = n
        + u * (mirror * (1 - 2 * (vertex_id as i32 & 1)))
        + v * (mirror * (1 - 2 * (vertex_id as i32 >> 1)));
    let xyz_10 = (xyz.as_f32() + Vec3::ONE) / 2.0;
    let v = chunk_backing_data.world_cells[chunk_id as usize]
        .truncate()
        .as_f32()
        + xyz_10;
    let p = v * CHUNK_SIZE as f32;
    *out_pos = global_data.world_to_projection * p.extend(1.0);
    *out_world_pos = p;
    *out_chunk_id = chunk_id;
}

#[spirv(fragment(depth_replacing), unroll_loops)]
pub fn raytrace(
    #[spirv(frag_coord)] frag_coord: Vec4,
    #[spirv(push_constant)] global_data: &RayTracingGlobalData,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] chunk_backing_data: &ChunkBackingData,
    #[spirv(uniform_constant, descriptor_set = 0, binding = 1)] atlas_in: &RuntimeArray<
        chunklib::AtlasIn,
    >,
    #[spirv(uniform_constant, descriptor_set = 0, binding = 2)] _atlas_out: &mut RuntimeArray<
        chunklib::AtlasOut,
    >,
    #[spirv(frag_depth)] depth_value: &mut f32,
    in_world_pos: Vec3,
    hit_normal: &mut Vec4,
    hit_cell: &mut IVec4,
    hit_offset: &mut Vec4,
    ray_dir: &mut Vec4,
) {
    #[cfg(target_arch = "spirv")]
    unsafe {
        asm!(
            "OpCapability RuntimeDescriptorArray",
            "OpExtension \"SPV_EXT_descriptor_indexing\""
        )
    }

    let mat_index = chunk_backing_data.atlases.material_data as usize;

    let zdir = global_data.inverse_direction;
    let direction = (zdir.xyz() - zdir.w * in_world_pos).normalize_or_zero();
    if direction == Vec3::ZERO {
        spirv_std::arch::kill();
    }

    let res = raytrace_generate!(
        global_data,
        chunk_backing_data,
        in_world_pos,
        direction,
        frag_coord.z,
        1.0 / frag_coord.w,
        hit_data,
        chunk,
        {
            let mat = chunk.fetch(hit_data.voxel, unsafe { &atlas_in.index(mat_index) });
            handle_material(
                mat,
                hit_data.origin + hit_data.voxel,
                hit_data.entry,
                hit_data.exit,
                hit_data.length,
                hit_data.feature,
                &mut hit_data,
            )
        }
    );

    match res {
        raylib::RaytraceData {
            result: raylib::RaytraceResult::Miss,
            ..
        } => spirv_std::arch::kill(),
        raylib::RaytraceData {
            result: raylib::RaytraceResult::Hit,
            normal,
            cell,
            offset,
            depth,
            time: _,
        } => {
            *hit_normal = ((normal + Vec3::ONE) / 2.0).extend(1.0);
            *hit_cell = cell.extend(0);
            *hit_offset = (offset * (depth * -2.0).exp()).extend(1.0);
            *depth_value = depth;
            *ray_dir = ((direction + Vec3::ONE) / 2.0).extend(1.0);
        }
    }
}

fn handle_material(
    material_info: Vec4,
    tip: IVec3,
    entry_point: Vec3,
    _exit_point: Vec3,
    _time_inside: f32,
    feature: BVec3,
    extra: &mut raylib::HitData,
) -> bool {
    match material_info.x as i32 {
        0 => false,
        _ => {
            let center = tip.as_f32() + Vec3::splat(0.5);
            let delta = entry_point - center;
            let normal = Vec3::select(feature, delta.signum(), Vec3::ZERO);
            extra.normal = normal;
            true
        }
    }
}

#[spirv(vertex)]
pub fn full_screen_quad(#[spirv(vertex_index)] vert_id: i32, out_pos: &mut Vec4) {
    *out_pos = vec4(
        ((vert_id / 2) * 2 - 1) as f32,
        (((vert_id % 2) ^ (vert_id / 2)) * 2 - 1) as f32,
        0.0,
        1.0,
    );
}
