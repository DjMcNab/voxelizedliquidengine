use glam::{IVec3, IVec4, UVec3, UVec4, Vec3, Vec4};

#[cfg(not(target_arch = "spirv"))]
#[allow(unused_imports)]
use spirv_std::macros::spirv;

use spirv_std::macros::Image;
use spirv_std::Sampler;

pub const CHUNK_SIZE: u32 = 16;
pub const MAX_CHUNKS: usize = 256;
pub const HASH_SIZE: usize = MAX_CHUNKS * 2;
pub const HASH_LEN: u32 = 10;
pub const MAX_ATLAS: usize = 10;

pub mod flags {
    pub const VISIBLE: u32 = 0x0001;
}

#[repr(C)]
#[derive(Clone)]
pub struct StandardAtlases {
    pub material_data: u32,
}

#[repr(C, align(16))]
#[derive(Clone)]
pub struct ChunkBackingData {
    pub world_cells: [IVec4; MAX_CHUNKS],
    pub atlas_origins: [UVec4; MAX_CHUNKS],
    pub chunk_flags: [u32; MAX_CHUNKS],
    pub hash_lookup: [u32; HASH_SIZE],
    pub num_chunks: u32,
    pub atlases: StandardAtlases,
}

pub type AtlasIn = Image!(3D, format=rgba32f, sampled=true);
pub type AtlasOut = Image!(3D, format=rgba32f, sampled=false);

// TODO: do a hash map or something

#[derive(Default)]
pub struct ChunkFound {
    pub id: u32,
    pub chunk_flags: u32,
    pub atlas_origin: UVec3,
}

#[inline]
pub fn hash_cell(v: IVec3) -> u32 {
    const FACTOR: u32 = 1103515245;
    let x = v.x as u32;
    let y = v.y as u32;
    let z = v.z as u32;
    x.wrapping_mul(FACTOR)
        .wrapping_add(y)
        .wrapping_mul(FACTOR)
        .wrapping_add(z)
        .wrapping_mul(FACTOR)
        .wrapping_add(13)
        % (HASH_SIZE as u32)
}

impl ChunkBackingData {
    pub fn find_chunk(&self, world_cell: IVec3, chunk_found: &mut ChunkFound) -> bool {
        let hash = hash_cell(world_cell);

        for offset in 0..HASH_LEN {
            let j = (hash + offset) % (HASH_SIZE as u32);
            let i = self.hash_lookup[j as usize];
            if i == u32::MAX {
                return false;
            }
            let i = i as usize;
            if self.world_cells[i].truncate() == world_cell {
                *chunk_found = ChunkFound {
                    id: i as u32,
                    chunk_flags: self.chunk_flags[i],
                    atlas_origin: self.atlas_origins[i].truncate(),
                };
                return true;
            }
        }
        false
    }

    pub fn get_chunk(&self, idx: u32) -> ChunkFound {
        if idx < self.num_chunks {
            return ChunkFound {
                id: idx,
                chunk_flags: self.chunk_flags[idx as usize],
                atlas_origin: self.atlas_origins[idx as usize].truncate(),
            };
        } else {
            panic!()
        }
    }
}

impl ChunkFound {
    #[spirv_std::macros::gpu_only]
    #[inline(always)]
    pub fn fetch(&self, coord: IVec3, atlas: &AtlasIn) -> Vec4 {
        atlas.fetch(self.atlas_origin.as_i32() + coord)
    }

    #[spirv_std::macros::gpu_only]
    #[inline(always)]
    pub fn trilinear_sample(&self, coord: Vec3, sampler: Sampler, atlas: &AtlasIn) -> Vec4 {
        let size = atlas.query_size_lod::<UVec3>(0).as_f32();
        let coord = self.atlas_origin.as_f32() + coord;
        atlas.sample_by_lod(sampler, coord / size, 0.0)
    }
}
