use glam::{BVec3, IVec3, Vec3};

#[macro_export]
macro_rules! raytrace_generate {
    ($gdata:ident, $chunks:ident, $pos:expr, $dir:expr, $z0:expr, $w0:expr, $hitdata:ident, $chunk:ident, $execute:expr) => {{
        use glam::{IVec3, UVec3, Vec3};
        use $crate::dda;
        use $crate::raylib::*;
        let position = $pos;
        let direction = $dir;
        let w0 = $w0;
        let z0 = $z0;
        let dwdt = $gdata
            .world_to_projection
            .transpose()
            .w_axis
            .truncate()
            .dot(direction);
        let dzdt = $gdata
            .world_to_projection
            .transpose()
            .z_axis
            .truncate()
            .dot(direction);

        let dwdt = if dwdt == 0.0 { 0.0 } else { dwdt };

        let max_time = 256.0; // TODO
        let time_at_which_lod_is_small = max_time.min((2.0 - w0) / dwdt);

        let ray = dda::Ray::new(position, direction);

        // A respectable offset to be able to skip first chunk.

        let mut chunk_dda = ray.dda(UVec3::splat(chunklib::CHUNK_SIZE), IVec3::ZERO, -1.0);

        // While LOD is big (screen-space W <= 2.0), step through with DDA
        let (normal, cell, offset, hit_time) = 'outer: loop {
            while chunk_dda.t <= time_at_which_lod_is_small {
                let hit = chunk_dda.next();
                let mut $chunk = Default::default();
                if $chunks.find_chunk(chunk_dda.quantized_tip, &mut $chunk) {
                    let origin = chunk_dda.quantized_tip * CHUNK_SIZE as i32;
                    let mut voxel_dda = ray.dda(UVec3::ONE, origin, (chunk_dda.t - 0.1).max(0.0));
                    let time_limit = time_at_which_lod_is_small.min(hit.exit_time);
                    let mut entry = ray.at(chunk_dda.t);
                    let mut exit;
                    while (any(voxel_dda.quantized_tip.cmplt(IVec3::ZERO))
                        || any(voxel_dda
                            .quantized_tip
                            .cmpge(IVec3::splat(CHUNK_SIZE as i32))))
                        && voxel_dda.t < time_limit
                    {
                        voxel_dda.advance(voxel_dda.next());
                    }

                    while voxel_dda.t < time_limit {
                        if any(voxel_dda.quantized_tip.cmplt(IVec3::ZERO))
                            || any(voxel_dda
                                .quantized_tip
                                .cmpge(IVec3::splat(CHUNK_SIZE as i32)))
                        {
                            break;
                        }
                        let hit = voxel_dda.next();
                        exit = ray.at(hit.exit_time);
                        let voxel = voxel_dda.quantized_tip;
                        let mut $hitdata = HitData {
                            origin,
                            voxel,
                            entry,
                            exit,
                            length: hit.exit_time - voxel_dda.t,
                            feature: voxel_dda.feature,
                            normal: Vec3::ZERO,
                        };
                        if { $execute } {
                            let tip = origin + voxel;
                            let offset: Vec3 = Vec3::from($hitdata.entry) - tip.as_f32();
                            break 'outer ($hitdata.normal, tip, offset, voxel_dda.t);
                        }
                        voxel_dda.advance(hit);
                        entry = exit;
                    }
                }
                chunk_dda.advance(hit);
            }
            // At small LODs, raymarch with simpler DDA algorithm, ramping up the step

            let smallest_time_step = 1.0 / direction.abs().max_element();
            while chunk_dda.t <= max_time {
                let hit = chunk_dda.next();
                let mut $chunk = Default::default();
                if $chunks.find_chunk(chunk_dda.quantized_tip, &mut $chunk) {
                    let origin = chunk_dda.quantized_tip * CHUNK_SIZE as i32;
                    let mut t = chunk_dda.t;
                    let time_limit = max_time.min(hit.exit_time);
                    let mut entry = ray.at(t);
                    let mut exit;
                    while t < time_limit {
                        let step = smallest_time_step * (w0 + dwdt * t) / 2.0;
                        exit = ray.at(t + step);
                        let tip = entry.floor().as_i32();
                        let voxel = tip - origin;
                        if any(voxel.cmplt(IVec3::ZERO))
                            || any(voxel.cmpge(IVec3::splat(CHUNK_SIZE as i32)))
                        {
                            continue;
                        }
                        let center = entry.floor() + Vec3::splat(0.5);
                        let delta = entry - center;
                        let delta_abs = delta.abs();
                        let mask = delta_abs.cmpeq(Vec3::splat(delta_abs.max_element()));
                        let mut $hitdata = HitData {
                            origin,
                            voxel,
                            entry,
                            exit,
                            length: step,
                            feature: mask,
                            normal: Vec3::ZERO,
                        };
                        if { $execute } {
                            let offset: Vec3 = Vec3::from($hitdata.entry) - tip.as_f32();
                            break 'outer ($hitdata.normal, tip, offset, t);
                        }

                        entry = exit;
                        t = t + step;
                    }
                }
                chunk_dda.advance(hit);
            }

            break 'outer (Vec3::ZERO, IVec3::ZERO, Vec3::ZERO, -1.0);
        };
        if hit_time >= 0.0 {
            let w = w0 + dwdt * hit_time;
            let z = z0 * w0 + dzdt * hit_time;
            let scaled_z = z / w;
            RaytraceData {
                result: RaytraceResult::Hit,
                normal,
                cell,
                offset,
                depth: scaled_z.max(z0),
                time: hit_time,
            }
        } else {
            RaytraceData {
                result: RaytraceResult::Miss,
                normal,
                cell,
                offset,
                depth: 1.0,
                time: hit_time,
            }
        }
    }};
}

#[cfg_attr(not(target_arch = "spirv"), derive(Debug))]
pub struct HitData {
    pub origin: IVec3,
    pub voxel: IVec3,
    pub entry: Vec3,
    pub exit: Vec3,
    pub length: f32,
    pub feature: BVec3,
    pub normal: Vec3,
}

pub enum RaytraceResult {
    Hit,
    Miss,
}

pub struct RaytraceData {
    pub result: RaytraceResult,
    pub normal: Vec3,
    pub cell: IVec3,
    pub offset: Vec3,
    pub depth: f32,
    pub time: f32,
}

#[cfg(target_arch = "spirv")]
pub use spirv_std::arch::any;
#[cfg(not(target_arch = "spirv"))]
pub fn any(x: BVec3) -> bool {
    x.any()
}
