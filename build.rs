use anyhow::*;
use glob::glob;
use spirv_builder::{Capability, MetadataPrintout, ModuleResult, SpirvBuilder};
use std::fs::{read_to_string, write};
use std::path::{Path, PathBuf};

struct ShaderData {
    src: String,
    src_path: PathBuf,
    as_path: PathBuf,
    spv_path: PathBuf,
    kind: shaderc::ShaderKind,
}

impl ShaderData {
    pub fn load(src_path: &Path, target_path: &Path) -> Result<Self> {
        let extension = src_path
            .extension()
            .context("File has no extension")?
            .to_str()
            .context("Extension cannot be converted to &str")?;
        let kind = match extension {
            "vert" => shaderc::ShaderKind::Vertex,
            "frag" => shaderc::ShaderKind::Fragment,
            "comp" => shaderc::ShaderKind::Compute,
            _ => bail!("Unsupported shader: {}", src_path.display()),
        };

        let src = read_to_string(src_path)?;
        let target_path = target_path.join(src_path.file_name().unwrap());
        let as_path = target_path.with_extension(format!("{}.s", extension));
        let spv_path = target_path.with_extension(format!("{}.spv", extension));

        Ok(Self {
            src,
            src_path: src_path.into(),
            as_path,
            spv_path,
            kind,
        })
    }
}

fn main() -> Result<()> {
    let out_dir = std::env::var_os("OUT_DIR").unwrap();
    let mut target_path = Path::new(&out_dir)
        .canonicalize()
        .context("OUT_DIR is invalid")?;
    target_path.push("shaders");
    std::fs::create_dir_all(&target_path).context("Cannot create shader path")?;
    // Collect all shaders recursively within /src/
    let mut shader_paths = [
        glob("./src/**/*.vert")?,
        glob("./src/**/*.frag")?,
        glob("./src/**/*.comp")?,
    ];

    // This could be parallelized
    let shaders = shader_paths
        .iter_mut()
        .flatten()
        .map(|glob_result| ShaderData::load(&glob_result?, target_path.as_ref()))
        .collect::<Vec<Result<_>>>()
        .into_iter()
        .collect::<Result<Vec<_>>>()?;

    let mut compiler = shaderc::Compiler::new().context("Unable to create shader compiler")?;
    let mut options = shaderc::CompileOptions::new().unwrap();
    options.set_include_callback(|path, incltype, origin, _| {
        eprintln!(
            "Resolving include for {:?} of type {:?} from {:?}",
            path, incltype, origin
        );
        if incltype == shaderc::IncludeType::Relative {
            let mut pb = PathBuf::from(origin);
            pb.pop();
            pb.push(path);
            let content = read_to_string(&pb).map_err(|o| o.to_string())?;
            Ok(shaderc::ResolvedInclude {
                resolved_name: pb.to_string_lossy().into_owned(),
                content,
            })
        } else {
            Err("invalid include".into())
        }
    });

    // This can't be parallelized. The [shaderc::Compiler] is not
    // thread safe. Also, it creates a lot of resources. You could
    // spawn multiple processes to handle this, but it would probably
    // be better just to only compile shaders that have been changed
    // recently.
    for shader in shaders {
        // This tells cargo to rerun this script if something in /src/ changes.
        println!(
            "cargo:rerun-if-changed={}",
            shader.src_path.as_os_str().to_str().unwrap()
        );

        eprintln!(
            "Compiling {} into {}",
            shader.src_path.to_string_lossy(),
            shader.spv_path.to_string_lossy()
        );

        let compiled = compiler
            .compile_into_spirv_assembly(
                &shader.src,
                shader.kind,
                &shader.src_path.to_str().unwrap(),
                "main",
                Some(&options),
            )?
            .as_text();
        let assembled = compiler.assemble(compiled.as_ref(), Some(&options))?;
        write(&shader.as_path, compiled)?;
        write(&shader.spv_path, assembled.as_binary_u8())?;
        println!(
            "cargo:rustc-env={}={}",
            shader.spv_path.file_name().unwrap().to_str().unwrap(),
            shader.spv_path.to_str().unwrap()
        );
    }
    let build_res = SpirvBuilder::new("./liquid_voxels_spir", "spirv-unknown-vulkan1.1")
        .bindless(false)
        .release(true)
        .capability(Capability::Int8)
        .capability(Capability::Int16)
        .capability(Capability::Int64)
        .capability(Capability::RuntimeDescriptorArray)
        .multimodule(true)
        .print_metadata(MetadataPrintout::DependencyOnly)
        .build()?;

    match build_res.module {
        ModuleResult::MultiModule(hm) => {
            for (ep, path) in hm.iter() {
                println!("cargo:rustc-env=entry_{}={}", &ep, path.to_str().unwrap());
            }
        }
        _ => unreachable!(),
    }

    Ok(())
}
